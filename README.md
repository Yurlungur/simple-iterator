simple_iterator

Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2014-05-06 14:28:07 (jonah)>

We include two programs to perform a polynomial evaluation to fill an
NxN matrix. One program is in C++ and the other is in speed-optimized
Python. To compare the run times, simply download the code, enter the
directory, and type:
    make

Dependancies:
    GNU C++ compiler
    Python2
    Numpy
    Scipy