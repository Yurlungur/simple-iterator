# Makefile for the simple_iterator package
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2014-05-06 15:12:07 (jonah)>

# The default compiler is g++
CXX = g++ -Ofast -march=native -g

default: speedtest
speedtest:
	@echo "compiling..."
	$(CXX) -o simple_iterator.bin simple_iterator.cpp
	@echo ""
	@echo "running c++ code"
	@echo ""
	./simple_iterator.bin
	@echo ""
	@echo "running python code"
	@echo ""
	python2 simple_iterator.py

clean:
	$(RM) simple_iterator.bin

.PHONY: default
