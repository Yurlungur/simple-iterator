#!/usr/bin/env python

"""
simple_iterator.py
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2014-05-06 15:11:38 (jonah)>

A little timing test to see if C++ or python are comparable for
filling an array.
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np
from numpy import sin
import time
from numpy.polynomial.hermite import hermval
# ----------------------------------------------------------------------

N = 2000

def combine(i,j):
    "Combines two integers i and j into a single double somehow."
    return sin(sin(sin(sin(sin(sin(sin(sin(sin(sin(sin(sin(sin(i+j)))))))))))))

def main():
    print "Testing the speed of filling an array."
    print "with the 5th Hermite polynomial."
    print "\t Array is {}x{}.".format(N,N)
    print ""
    
    start1 = time.time()
    my_array = np.fromfunction(combine,(N,N),dtype=np.float64)
    end1 = time.time()
    diff1 = end1 - start1
    
    print "The total runtime was:\n\t {} seconds.".format(diff1)
    print "One element of the array is:\n\t {0:.64f}".format(my_array[N/2,N/2])
    return

if __name__ == "__main__":
    main()
