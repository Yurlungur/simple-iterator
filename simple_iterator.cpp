// simple_iterator.cpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-05-06 15:19:45 (jonah)>

// A little timing test to see if C++ or python are comparable for
// filling an array.

// ----------------------------------------------------------------------

// Include
#include <iostream>
#include <stdio.h> // easier than mucking about in iostream precisions
#include <cmath>
#include <ctime>
#include <vector>
using namespace std;

// Important constants
const int N = 2000;


// Prototypes
// ----------------------------------------------------------------------
// Combines two integers i and j into a single double somehow.
double combine(int i, int j);
// ----------------------------------------------------------------------


// main function
// ----------------------------------------------------------------------
int main() {
   clock_t t1,t2; // initial and final cpu times
   double diff; // The difference in cpu cycles
   double seconds; // The difference in times in seconds
   // Declare test array
   vector<double> testA(N*N, 1.0);

   cout << "Testing the speed of filling an arryay\n"
        << "with the 5th Hermite polynomial.\n"
        << "\t Array is " << N << "x" << N << ".\n"
        << endl;
   t1 = clock();
   
   // The loop
   for (int i = 0; i < N; i++) {
     for (int j = 0; j < N; j++) {
       testA[i*N+j] = combine(i,j);
     }
   }
   
   t2 = clock();
   seconds = double(t2-t1)/double(CLOCKS_PER_SEC);
   
   cout << "The total runtime was" << endl;
   printf("\t %.32f",seconds);
   cout << endl;

   cout << "One element of the array is:" << endl;
   printf("\t %.64f",testA[(N/2)*N + (N/2)]);
   cout << endl;
   
   return 0;
}
// ----------------------------------------------------------------------


// Implementations
// ----------------------------------------------------------------------
double combine(int i, int j) {
  return sin(sin(sin(sin(sin(sin(sin(sin(sin(sin(sin(sin(sin(i+j)))))))))))));
}
// ----------------------------------------------------------------------

